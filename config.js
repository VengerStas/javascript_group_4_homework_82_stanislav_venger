const path = require('path');

const rootPath = __dirname;


module.exports = {
    rootPath,
    uploadPathArtist: path.join(rootPath, 'public/uploads/artist'),
    uploadPathAlbum: path.join(rootPath, 'public/uploads/album'),
    dbUrl: 'mongodb://localhost/radio',
    mongoOption: { userNewUrlParser: true}
};