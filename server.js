const express = require('express');
const cors = require ('cors');
const mongoose = require ('mongoose');
const config = require('./config');
const artist = require('./app/artist');
const album = require ('./app/album');
const track = require ('./app/track');

const app = express();
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOption).then(() => {
   app.use('/artist', artist);
   app.use('/album', album);
   app.use('/track', track);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});